### GitLab.org Branch Build Statuses

#### GitLab Community Edition

##### Master

Build Status: ![build](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)
Coverage: ![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg)

##### 9-0-Stable

Build Status: ![build](https://gitlab.com/gitlab-org/gitlab-ce/badges/9-0-stable/build.svg)
Coverage: ![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/9-0-stable/coverage.svg)

#### GitLab Enterprise Edition

##### Master

Build Status: ![build](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/build.svg)
Coverage: ![coverage](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/coverage.svg)

##### 9-0-Stable-EE

Build Status: ![build](https://gitlab.com/gitlab-org/gitlab-ee/badges/9-0-stable-ee/build.svg)
Coverage: ![coverage](https://gitlab.com/gitlab-org/gitlab-ee/badges/9-0-stable-ee/coverage.svg)
